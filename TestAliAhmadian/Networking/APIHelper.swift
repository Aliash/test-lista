////
////  APIHelper.swift
////  TestAliAhmadian
////
////  Created by Ali Ahmadian shalchi on 07/03/2020.
////  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
////
//
//import Foundation
//import UIKit
//import Alamofire
//
//enum EndPoint: String {
//    case list = ""
//    
//    var type: HTTPMethod {
//    switch self {
//    case .list:
//        return .get
//        }
//    }
//    
//}
//
//
//
//enum testError: Error {
//    case genericError
//}
//
//enum Enviroment: String {
//    case mock = "http://private-f0eea-mobilegllatam.apiary-mock.com/list"
//    case dev = "dev"
//    case staging = "staging"
//    case prod = "prod"
//}
//
//class API {
//    
//    static var baseURL: Enviroment = .mock
//    
//    static func fetch<R: Decodable>(endPoint: EndPoint, returnType: R.Type, parameters: [String: Any], onSeccess: @escaping (R) -> (), onError: @escaping (Error) -> ()) {
//        Alamofire.request(baseURL.rawValue + endPoint.rawValue, method: endPoint.type, parameters: parameters, encoding: JSONEncoding.default , headers: [:])
//        .validate()
//            .responseData { (response) in
//                switch response.result {
//                    
//                case .success:
//                    do {
//                        if let data = response.result.value {
//                            let results = try JSONDecoder().decode(returnType.self, from: data)
//                            onSeccess(results)
//                        } else {
//                            onError(testError.genericError)
//                        }
//                    }
//                    catch let error {
//                        print("Found an error - \(error)")
//                        onError(testError.genericError)
//                    }
//                case .failure(let error):
//                    print(error)
//                }
//        }
//    }
//    
//}
