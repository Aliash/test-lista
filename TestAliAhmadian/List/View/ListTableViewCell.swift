//
//  ListTableViewCell.swift
//  TestAliAhmadian
//
//  Created by Ali Ahmadian shalchi on 08/03/2020.
//  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var laptopImage: UIImageView!
    @IBOutlet weak var laptopTitle: UILabel!
    @IBOutlet weak var laptopDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(_ laptop: LaptopResponse) {
        self.laptopTitle.text = laptop.title
        self.laptopDescription.text = laptop.description
        
        if let imageURL = URL(string: laptop.image) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.laptopImage.image = image
                    }
                }
            }
        }
    }

}
