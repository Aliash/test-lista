//
//  DetailsViewController.swift
//  TestAliAhmadian
//
//  Created by Ali Ahmadian shalchi on 09/03/2020.
//  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var laptopDetailedImage: UIImageView!
    @IBOutlet weak var laptopDetailedTitle: UILabel!
    @IBOutlet weak var laptopDetailedTextView: UITextView!
    
    var laptop: LaptopResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        laptopDetailedTextView.text = laptop?.description
        laptopDetailedTitle.text = laptop?.title
//        laptopDetailedImage.image = laptop?.image
        
//        if let image = laptop?.image {
//            let imagetoShow = UIImage(data: image)
//            self.laptopDetailedImage.image = image
//        }
        
    }
    
    
    
}
