//
//  ListViewController.swift
//  TestAliAhmadian
//
//  Created by Ali Ahmadian shalchi on 03/03/2020.
//  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
//

import UIKit
import Foundation

class ListViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    let url = URL(string: "https://private-f0eea-mobilegllatam.apiary-mock.com/list")
    private var laptops = [LaptopResponse]()
    
    var rowToshow: LaptopResponse? {
        didSet {
            performSegue(withIdentifier: "showDetails", sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadJson {
            self.listTableView.reloadData()
        }
        
        listTableView.tableFooterView = UIView()
    }
    
    func downloadJson(completed: @escaping () -> () ) {
        
        guard let downloadURL = url else { return }
        
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            
            guard let data = data, error == nil, urlResponse != nil else {
                print("something is wrong")
                return
            }
            print("downloaded")
            
            do {
                let downloadedLaptops = try JSONDecoder().decode([LaptopResponse].self, from: data)
                self.laptops = downloadedLaptops
                DispatchQueue.main.async {
                    self.listTableView.reloadData()
                }
            } catch {
                print("something wrong after downloaded")
            }
            
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailsViewController {
            destination.laptop = rowToshow
        }
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ listTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laptops.count
    }
    
    func tableView(_ listTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = listTableView.dequeueReusableCell(withIdentifier: "ListCell") as? ListTableViewCell else { return UITableViewCell() }
        cell.setup(laptops[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        performSegue(withIdentifier: "showDetails", sender: self)
        rowToshow = laptops[indexPath.row]
    }
    
}
