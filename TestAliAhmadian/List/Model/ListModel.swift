//
//  ListModel.swift
//  TestAliAhmadian
//
//  Created by Ali Ahmadian shalchi on 03/03/2020.
//  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
//

import UIKit

class LaptopResponse: Decodable {
    let title: String
    let description: String
    let image: String

    init(title: String , description: String , image: String) {
        self.title = title
        self.description = description
        self.image = image
    }
}




