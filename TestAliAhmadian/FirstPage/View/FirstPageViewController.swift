//
//  ViewController.swift
//  TestAliAhmadian
//
//  Created by Ali Ahmadian shalchi on 03/03/2020.
//  Copyright © 2020 Ali Ahmadian shalchi. All rights reserved.
//

import UIKit

class FirstPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }

    @IBAction func goToListas(_ sender: Any) {
        self.performSegue(withIdentifier: "goToListSuccess", sender: self)
    }
    
    @IBAction func goToAboutAli(_ sender: Any) {
        self.performSegue(withIdentifier: "goToAboutAliSuccess", sender: self)
    }
    
    @IBAction func goToContact(_ sender: Any) {
        self.performSegue(withIdentifier: "goToContactSuccess", sender: self)
    }
}

